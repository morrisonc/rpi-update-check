# rpi-update-check

A automatic check for rpi-update on your Raspberry Pi

## License

[![Creative Commons BY-SA][2]][1]

## Maintainer
+ Christoph Jeschke 
    * [post@christoph-jeschke.de]([3])
    * GPG [`F425 19F7 4D7F CF80 9ADD 90B9 F85E 6264 EEFF 484B`]([4])
    
    
[1]: https://creativecommons.org/licenses/by-sa/4.0/
[2]: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
[3]: mailto:post@christoph-jeschke.de
[4]: http://christoph-jeschke.de/EEFF484B-pubkey.asc